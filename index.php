<?php 
header("Content-type: text/html; charset=utf-8");
require_once 'includes/init.php';

//数据库表
$sql = "SELECT TABLE_NAME,TABLE_COMMENT,`ENGINE` FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '{$database}'";
$table_name = $db->getAll($sql);  
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title ?></title>
        <style type="text/css">  
            * {  
                margin: 0px;  
                padding: 0px; 
                font-size: 16px;
                font-family: 微软雅黑; 
            }  
            html,body,.mainbox{
                position: relative;
                width: 100%;
                height: 100%;
            }
            .mainbox header{
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                height: 55px;
                background: blue;
                z-index: 3;
                line-height: 55px;
            }
            .mainbox .left{
               	position: absolute;
                top: 0;
                right: 0;
                height: 100%;
                width: 40%;
                z-index: 2;
               	background-color:#fff;
               	overflow-y:scroll;
            }
            .mainbox .right{
				position: absolute;
                left: 0;
                padding-right: 40%;
                background: #efe9e9;
                width: 100%;
                height: 100%;
                box-sizing:border-box;
                overflow: hidden;  
            }
            .mainbox .right .rightbox{
                /*overflow-y:scroll;*/
            }
            .mainbox .right .rightbox .rightcon p{
                text-align: center;
            }
            footer{
                background: #000;
                height: 40px;
                line-height: 40px;
                width: 100%;
                position: absolute;
                bottom: 0;
                z-index: 5;
            }
            footer p,header p,.left p{
                text-align: center;
                font-size: 16px;
                color: #fff;
            }
            .left p{
                margin-top: 300px;
            }
            .dataTables_wrapper .dataTables_filter {
			    float: right;
			    text-align: right;
			    margin-top: 26px; 
			}
        </style> 
         <link rel="stylesheet" href="__PUBLIC__/css/bootstrap.min.css">
    </head>
    <body>
        <div class="mainbox">  
        	<div class="right" id="right">
                <iframe  id="iframeSon" src="tableInfo.php?name=<?php echo $database ?>" frameborder="0" scrolling="yes" width="100%" height="100%"></iframe>
            </div>
    	  	<div class="left">
	  		  	<button class="btn btn-warning " id="wordAll" style="float: left;margin-left: 8px;margin-top: 26px; ">合并导出Word文档</button>
	  		   	<!-- <a href="WordTable.php?tableName="><button class="btn btn-green " style="float: left;margin-left: 5px;margin-top: 26px; ">合并导出Excel文档</button></a> -->
               	<table class="table table-hover" id="example">
			      <thead>
			      <tr>
			          <th colspan="4" style="text-align: center;"><?php echo $title ?></th>
			        </tr>
			        <tr>
			          <th><input type="checkbox" id="checkall"></th>
			          <th>#</th>
			          <th>表名</th>
			          <th>注释</th>
			          <th>数据引擎</th>
			        </tr>
			      </thead>
			      <tbody>
			       <?php foreach ($table_name as $key => $v) { ?>
			        <tr>
		        	  <th scope="row"><input type="checkbox" data="<?php echo $v['TABLE_NAME'] ?>"></th>
			          <td><?php echo $key++ ?></th>
			          <td onclick="fram('<?php echo $v['TABLE_NAME'] ?>')"><?php echo $v['TABLE_NAME'] ?></td>
			          <td><?php echo $v['TABLE_COMMENT'] ?></td>
			          <td><?php echo $v['ENGINE'] ?></td>
			        </tr>
			      <?php } ?>
			      </tbody>
			    </table>
		    </div>
        </div> 
        <script type="text/javascript" language="javascript" src="//cdn.staticfile.org/jquery/3.4.1/jquery.js"></script>
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
       	<script>
     	$(document).ready(function(){
	     	$('#example').dataTable({
	         	"bPaginate": false, //翻页功能
				"bLengthChange": false, //改变每页显示数据数量
				"bFilter": true, //过滤功能
				"bSort": true, //排序功能
				"bInfo": true,//页脚信息
				"bAutoWidth": true,//自动宽度
				"language": {
			        "sProcessing": "处理中...",
			        "sLengthMenu": "显示 _MENU_ 项结果",
			        "sZeroRecords": "没有匹配结果",
			        "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
			        "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
			        "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
			        "sInfoPostFix": "",
			        "sSearch": "搜索:",
			        "sUrl": "",
			        "sEmptyTable": "表中数据为空",
			        "sLoadingRecords": "载入中...",
				},
     		});
     		//复选框
 			var i=0;
 		 	$("#checkall").on("click",function(){
                if(i==0){
                    //把所有复选框选中
                    $("#example th :checkbox").prop("checked", true);
                    i=1;
                }else{
                    $("#example th :checkbox").prop("checked", false);
                    i=0;
                }
                
            });
            $("#wordAll").on("click",function(){
            	var tableName= new Array() ;
            	var database = "<?php echo $database ?>";
                $("#example th :checkbox:checked").each(function(i,o){
                	tableName.push($(o).attr("data"));
                }) 
                if(tableName.length == 0){
                	alert('请选择数据表');
                }else{
                	post('WordTable.php',{table_name:tableName,name:database});
                }
                
            });
     		function post(URL, PARAMS) {        
			    var temp = document.createElement("form");        
			    temp.action = URL;        
			    temp.method = "post";        
			    temp.style.display = "none";        
			    for (var x in PARAMS) {        
			        var opt = document.createElement("textarea");        
			        opt.name = x;        
			        opt.value = PARAMS[x];               
			        temp.appendChild(opt);        
			    }        
			    document.body.appendChild(temp);        
			    temp.submit(); 
			    return temp; 
			}
			
		});
		function fram(tableName){
			var childWindow = $("#iframeSon")[0].contentWindow; //表示获取了嵌入在iframe中的子页面的window对象。  []将JQuery对象转成DOM对象，用DOM对象的contentWindow获取子页面window对象。
			childWindow.framtop(tableName);
		}
		</script>
    </body>
</html>
