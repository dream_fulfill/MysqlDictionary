<?php
// 接值类
class Request{
    // post接值
    static function rPost($name,$content=''){
        return isset($_POST[$name])?$_POST[$name]:$content;
    }
    // get方式接值
    static function rGet($name,$content=''){
        return isset($_GET[$name])?$_GET[$name]:$content;
    }
    // request接值
    static function rRequest($name,$content=''){
        return isset($_REQUEST[$name])?$_REQUEST[$name]:$content;
    }
}