<?php
// 开始时间
// $start=time()+microtime();
// 总引文 所有文件的公共文件
header('content-type:text/html;charset=utf-8');
// 通过程序找到网站根目录的路径
define('ROOTPATH',str_replace("\\","/",dirname(__DIR__)));
// 通过根目录取引入所有的类库包
include ROOTPATH."/includes/Mysql.class.php";
include ROOTPATH."/includes/Page.class.php";
include ROOTPATH."/includes/Request.class.php";
include ROOTPATH."/includes/function.php";

// 配置数据库 （填写对应的数据库 地址、账号、密码）
$dbserver = "127.0.0.1";
$dbport = "3306";
$dbusername = "root";
$dbpassword = "root";

$database = $_REQUEST['name'];

if(empty($database)){
    exit("请输入数据库名，规范: {$_SERVER['HTTP_HOST']}/?name=databasename");
}

// //数据库对象
$db=new Mysql($database,$dbserver,$dbusername,$dbpassword);
//数据库名称
$title = "数据库{$database}数据字典";
