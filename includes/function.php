<?php
// 自定义函数库
// 成功的跳页
function success($msg,$url){
    echo "<script>
    alert('{$msg}');
    location.href='{$url}';
    </script>";
    exit;
}
function top_success($msg,$url){
    echo "<script>
    alert('{$msg}');
    top.location.href='{$url}';
    </script>";
    exit;
}
// 失败的跳页
function error($msg){
    echo "<script>
    alert('{$msg}');
    history.back();
    </script>";
    exit;
}
function download($act){
    if($act!=''){
        // 下载功能
        //文件是否存在
        if(file_exists($act)){
            //执行下载功能
            //获取文件大小
            $size=filesize($act);
            //  浏览器以信息形式接收
            header('content-type:application/octet-stream');
            //  接收单位
            header('Accept-Ranges:bytes');
            //  接收文件大小
            header('Accept-Length:'.$size);
            //  以附件形式接收
            $arr=explode(".", $act);
            $type=end($arr);
            $rand=date('Ymd').mt_rand(100000, 999999);
            header("content-disposition:attachment;filename={$rand}.{$type}");
            //  读取内容
            readfile($act);
        }else{
            echo "<script>
			alert('文件不存在');
			history.back();
			</script>";
        }
        exit;
    }
}