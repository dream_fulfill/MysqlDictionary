<?php 
header("Content-type: text/html; charset=utf-8");
require_once 'includes/init.php';

ini_set('max_execution_time', '300');

//数据库表
$sql = "SELECT TABLE_NAME,TABLE_COMMENT,`ENGINE` FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '{$database}'";
$table_name = $db->getAll($sql);
//表内容
foreach ($table_name as $key => $v) {
  $sql = "SELECT COLUMN_NAME,COLUMN_TYPE,COLUMN_DEFAULT,IS_NULLABLE,EXTRA,COLUMN_COMMENT,COLUMN_KEY FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{$v['TABLE_NAME']}' AND table_schema = '{$database}'";

  $column_name = $db->getAll($sql);

  $table_name[$key]['column_name'] = $column_name;
}      
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>MySQL数据表详情</title>
    <link href="__PUBLIC__/css/bootstrap.min.css" rel="stylesheet">
    <link href="__PUBLIC__/css/style.min.css" rel="stylesheet">
</head>
<body class="gray-bg">
<script type="text/javascript" language="javascript" src="//cdn.staticfile.org/jquery/3.4.1/jquery.js"></script>
<script>
  function framtop(tableName) {
      var scroll_offset = $('#'+tableName).offset(); //得到box这个div层的offset，包含两个值，top和left
      $("body,html").animate({
          scrollTop:scroll_offset.top //让body的scrollTop等于pos的top，就实现了滚动
      })
  }
</script>
<div class="wrapper-content ">
    <div class="col-sm-12">
        <?php foreach ($table_name as $key => $v) { ?>
              <div class="ibox-title">               
                <h5 id="<?php echo $v['TABLE_NAME'] ?>" >
                      <?php echo $v['TABLE_NAME'] .'&nbsp;'. $v['TABLE_COMMENT'] .'&nbsp;'. $v['ENGINE']?>
                </h5>  
                <!-- <button class="btn btn-primary btn-xs" id="exportExcel"> 导出 </button>  -->
                <a href="WordTable.php?tableName=<?php echo $v['TABLE_NAME'] ?>">
                  <button class="btn btn-warning btn-xs" style="float: right;margin-right: 5px">导出Word文档</button>
                </a>
              </div>
              <div class="ibox-content" style="overflow-x:scroll">
                  <table class="table  table-striped table-bordered table-hover" id="example" >
                      <thead>
                      <tr>
                          <th>字段名</th>
                          <th>数据类型</th>
                          <th>默认值</th>
                          <th>允许非空</th>
                          <th>自动递增</th>
                          <th>备注</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($v['column_name'] as  $vv) {?>
                          <tr>
                            <th><?php echo $vv['COLUMN_NAME']  ?></th>
                            <td><?php echo $vv['COLUMN_TYPE'] ?></td>
                            <td><?php echo $vv['COLUMN_DEFAULT']  ?></td>
                            <td><?php echo $vv['IS_NULLABLE']  ?></td>
                            <td><?php echo $vv['EXTRA'] == 'auto_increment' ? '是' : '&nbsp;' ?></td>
                            <td><?php echo $vv['COLUMN_KEY'] == 'PRI' ? '(主键)' :''; echo $vv['COLUMN_KEY'] == 'MUL' ? '(索引)' : ''; echo $vv['COLUMN_COMMENT']; ?></td>
                          </tr>
                        <?php } ?>    
                      <tbody>
                  </table>
              </div>     
      <?php } ?>
    </div>
</div>
</body>
</html>
