<?php
require_once 'PHPWord.php';
require_once 'includes/init.php';
//接收参数
$table_name = isset($_GET['tableName'])?$_GET['tableName']:$_POST['table_name'];
date_default_timezone_set("PRC");
/*******************************PHPword导出***********************************************************************/
if(isset($_POST['table_name'])){
	//文件名
	$title = "数据库{$database}";
	$table_name = str_replace(",","','",$table_name);
	//数据库表数据
	$sql = "SELECT TABLE_NAME,TABLE_COMMENT,`ENGINE` FROM INFORMATION_SCHEMA.TABLES WHERE table_name in ('{$table_name}')  AND table_schema = '{$database}'";
	//结果集
	$table_name = $db->getAll($sql);
}else{
	//文件名
	$title = "数据库{$database}表名{$table_name}";
	//数据库表数据
	$sql = "SELECT TABLE_NAME,TABLE_COMMENT,`ENGINE` FROM INFORMATION_SCHEMA.TABLES WHERE table_name = '{$table_name}'  AND table_schema = '{$database}'";
	//结果集
	$table_name = $db->getAll($sql);
}


/*******************************PHPword导出***********************************************************************/

$PHPWord = new PHPWord();
//表头样式
$PHPWord->addFontStyle('rStyle', array('bold'=>true,'size'=>12));
$PHPWord->addParagraphStyle('pStyle', array('spaceAfter'=>80));

//表格样式
$styleTable = array('borderSize'=>2, 'borderColor'=>'006699', 'cellMargin'=>20);
$styleFirstRow = array('borderBottomSize'=>2, 'borderBottomColor'=>'0000FF', 'bgColor'=>'66BBFF');


// 表格第一行样式
$styleCell = array('valign'=>'center');
$styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);


// 文字样式
$fontStyle = array('bold'=>true, 'align'=>'center');
//创建页面
$section = $PHPWord->createSection();
//循环导出数据
foreach ($table_name as $key => $v) {

	//表头文字
	$section->addText(iconv('utf-8','GBK//IGNORE',"表名称：{$v['TABLE_NAME']}  数据引擎：{$v['ENGINE']}"),'rStyle');
	$xian = '---------------------------------------------------------------------------------------------------------------------------------------';
	$section->addText($xian,array('color'=>'006699'));
	$comment = empty($v['TABLE_COMMENT'])?'- 无数据表注释':'- '. $v['TABLE_COMMENT'];
	$section->addText(iconv('utf-8','GBK//IGNORE',$comment),'rStyle','pStyle');

	// 创建表格样式
	$PHPWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

	// 创建表格
	$table = $section->addTable('myOwnTableStyle');

	// 表格第一行
	$table->addRow(90);

	// 表格第一个内容
	$table->addCell(2000, $styleCell)->addText(iconv('utf-8','GBK//IGNORE','字段名'), $fontStyle);
	$table->addCell(2000, $styleCell)->addText(iconv('utf-8','GBK//IGNORE','数据类型'), $fontStyle);
	$table->addCell(2000, $styleCell)->addText(iconv('utf-8','GBK//IGNORE','默认值'), $fontStyle);
	$table->addCell(500, $styleCell)->addText(iconv('utf-8','GBK//IGNORE','允许非空'), $fontStyle);
	$table->addCell(500, $styleCell)->addText(iconv('utf-8','GBK//IGNORE','自动递增'), $fontStyle);
	$table->addCell(2000, $styleCell)->addText(iconv('utf-8','GBK//IGNORE','备注'), $fontStyle);
	//数据表详细内容
	$sql = "SELECT COLUMN_NAME,COLUMN_TYPE,COLUMN_DEFAULT,IS_NULLABLE,EXTRA,COLUMN_COMMENT,COLUMN_KEY FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{$v['TABLE_NAME']}' AND table_schema = '{$database}'";
	$column_name = $db->getAll($sql); 
	// 表格生育行数内容
	foreach($column_name as $c){
		$table->addRow();
		$table->addCell(2000)->addText($c['COLUMN_NAME']);
		$table->addCell(2000)->addText($c['COLUMN_TYPE']);
		$table->addCell(2000)->addText(iconv('utf-8','GBK//IGNORE',$c['COLUMN_DEFAULT']));
		$table->addCell(500)->addText($c['IS_NULLABLE']);
		$table->addCell(500)->addText(iconv('utf-8','GBK//IGNORE',$c['EXTRA'] == 'auto_increment' ? '是' : '否'));
		$key = $c['COLUMN_KEY'] == 'PRI' ? '(主键)' : '';
		$key.= $c['COLUMN_KEY'] == 'MUL' ? '(索引)' : '';
		$key.= $c['COLUMN_COMMENT'];
		$table->addCell(2000)->addText(iconv('utf-8','GBK//IGNORE',$key));
	}
	$section->addTextBreak(1);
}
	

$fileName = date('Y-m-d')."_{$title}.docx";

ob_end_clean();//清除缓冲区,避免乱码
header('Content-Type: application/msword');
header("Content-Disposition: attachment;filename=\"$fileName\"");
header('Cache-Control: max-age=0');

// 创建文件
$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
$objWriter->save('php://output');
?>
